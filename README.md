# Desafio_distancia

Descobrir a distância em metros entre um lugar até o metrô/trem mais perto.
usando a formula da distância entre dois pontos, depois convertendo 
distância em graus para milhas náuticas, depois para metros.
foi utilizado o spark-shell com dois DF, um para os locais e outro 
para as estações. 
Desafio realizado utilizando os DF com cross join e depois criando colunas 
para descobrir as distâncias
Os comandos executados se encontram no arquivo chamado codigos
